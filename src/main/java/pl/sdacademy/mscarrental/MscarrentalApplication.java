package pl.sdacademy.mscarrental;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MscarrentalApplication {

    public static void main(String[] args) {
        SpringApplication.run(MscarrentalApplication.class, args);
    }

}
